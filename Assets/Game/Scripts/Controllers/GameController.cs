﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Controllers;
using Sdn.Core.Data;
using Sdn.Core.Managers;

public class GameController : Controller
{

    private RoomController roomController;
    private PlayerController playerController;
    private TurnController turnController;
    private VisitorController visitorController;
    private CameraController cameraController;
    private ScareController scareController;
    private JobController jobController;

    private GameData gameData;
    private GameSettingsData settingsData;
    private GameSessionData sessionData;

    public Scare PrimaryScare { get; set; }
    public Scare SecondaryScare { get; set; }
    public Scare AltScare1 { get; set; }
    public Scare AltScare2 { get; set; }


    public RoomController RoomController
    {
        get { return roomController; }
    }

    public PlayerController PlayerController
    {
        get { return playerController; }
    }

    public TurnController TurnController
    {
        get { return turnController; }
    }

    public VisitorController VisitorController
    {
        get { return visitorController; }
    }

    public CameraController CameraController
    {
        get { return cameraController; }
    }

    public ScareController ScareController
    {
        get { return scareController; }
    }

    public JobController JobController
    {
        get { return jobController; }
    }

    public GameData GameData
    {
        get { return gameData; }
    }

    public GameSessionData SessionData
    {
        get { return sessionData; }
    }

    public GameSettingsData SettingsData
    {
        get { return settingsData; }
    }

    public override void ControllerInit()
    {
        D.Trace("[GameController] ControllerInit");


        scareController = GameObject.FindObjectOfType<ScareController>();

        cameraController = GameObject.FindObjectOfType<CameraController>();
        turnController = GameObject.FindObjectOfType<TurnController>();
        roomController = GameObject.FindObjectOfType<RoomController>();
        playerController = GameObject.FindObjectOfType<PlayerController>();
        jobController = GameObject.FindObjectOfType<JobController>();
        visitorController = GameObject.FindObjectOfType<VisitorController>();

        scareController.ControllerInit();

        PrimaryScare = scareController.CreateScare(sessionData.PrimaryScare);
        PrimaryScare.GameController = this;
        PrimaryScare.ScareInit();
        SecondaryScare = scareController.CreateScare(sessionData.SecondaryScare);
        SecondaryScare.GameController = this;
        SecondaryScare.ScareInit();
        AltScare1 = scareController.CreateScare(sessionData.AltScare1);
        AltScare1.GameController = this;
        AltScare1.ScareInit();
        AltScare2 = scareController.CreateScare(sessionData.AltScare2);
        AltScare2.GameController = this;
        AltScare2.ScareInit();

        playerController.ControllerInit();
        cameraController.ControllerInit();
        roomController.ControllerInit();
        turnController.ControllerInit();

    }

    public override void ControllerStart()
    {
        D.Trace("[GameController] ControllerStart");
        jobController.ControllerStart();
        playerController.ControllerStart();
        cameraController.ControllerStart();
        roomController.ControllerStart();
        turnController.ControllerStart();
    }

    public void NextLevel()
    {
        turnController.ControllerStop();
        roomController.ControllerStop();
        cameraController.ControllerStop();
        playerController.ControllerStop();
        jobController.ControllerStop();

        jobController.NextJob();
    }

    public override void OnEnable()
    {
        D.Trace("[GameController] OnEnable");
        gameData = (GameData)GameManager.Instance.Data;
        settingsData = (GameSettingsData)GameManager.Instance.Settings;
        sessionData = settingsData.SessionData1;
    }
}
