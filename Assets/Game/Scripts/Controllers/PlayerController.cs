﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Controllers;

public class PlayerController : Controller
{
    private Player player;

    public Player Player
    {
        get { return player; }
    }

    public override void ControllerInit()
    {
        D.Trace("[PlayerController] ControllerInit");
        player = GameObject.FindObjectOfType<Player>();

    }
}
