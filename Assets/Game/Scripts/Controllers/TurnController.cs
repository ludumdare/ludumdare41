﻿using Sdn.Core.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System.Linq;
using UnityEngine.Profiling;
using UnityEngine.SceneManagement;

public class TurnController : Controller
{
    private IList<Turnable> turnables;
    private IList<Turnable> sortedTurnables;
    private Turnable playerTurnable;
    private GameController gameController;
    private int turnNumber;
    private Coroutine taskRunner;

    public void AddTurnable(Turnable turnable)
    {
        turnable.TurnableInit();
        turnables.Add(turnable);
    }

    public int TurnNumber
    {
        get { return turnNumber; }
    }


    public override void ControllerInit()
    {
        D.Trace("[TurnController] ControllerInit");

        gameController = GameObject.FindObjectOfType<GameController>();
        
        turnables = GameObject.FindObjectsOfType<Turnable>().ToList<Turnable>();
        sortedTurnables = turnables.OrderBy(o => o.TurnOrder).ToList<Turnable>();

        foreach(Turnable turnable in sortedTurnables)
        {
            turnable.GameController = gameController;
            turnable.TurnableInit();
        }

        playerTurnable = (Turnable)GameObject.FindObjectOfType<Player>().GetComponent<Turnable>();

    }

    public override void ControllerStart()
    {
        D.Trace("[TurnController] ControllerStart");
        taskRunner = StartCoroutine(runner());
    }

    private IEnumerator runner()
    {
        D.Trace("[TurnController] runner");

        while (true)
        {
            yield return StartCoroutine(nextTurn());

            Visitor[] visitors = GameObject.FindObjectsOfType<Visitor>();
            int escaped = 0;
            foreach(Visitor visitor in visitors)
            {
                if (visitor.State == Visitor.VisitorStates.STATE_ESCAPING)
                    escaped += 1;
            }

            int remaining = gameController.VisitorController.Visitors - escaped;

            D.Trace("- visitors remaining: {0}, escaped: {1}", remaining, escaped);

            if (remaining <= 0)
            {
                gameController.NextLevel();
            }
            yield return null;
        }
    }

    private IEnumerator nextTurn()
    {
        D.Trace("[TurnController] nextTurn");
        Profiler.BeginSample("nextTurn");

        turnNumber += 1;

        sortedTurnables = turnables.OrderBy(o => o.TurnOrder).ToList<Turnable>();

        foreach (Turnable turnable in sortedTurnables)
        {
            if (turnable.IsActive)
            {
                yield return StartCoroutine(turnable.TurnableBeforeTurn());
            }
        }
        foreach (Turnable turnable in sortedTurnables)
        {
            if (turnable.IsActive)
            {
                yield return StartCoroutine(turnable.TurnableTurn());
            }
        }
        foreach (Turnable turnable in sortedTurnables)
        {
            if (turnable.IsActive)
            {
                yield return StartCoroutine(turnable.TurnableAfterTurn());
            }
        }
        Profiler.EndSample();
        yield return null;
    }
}
