﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Controllers;
using UnityEngine.SceneManagement;
using Sdn.Core;
using Sdn.Core.Managers;

public class JobController : Controller
{
    public JobBoardData jobBoards;

    private GameController gameController;
    private GameSessionData sessionData;

    private JobData jobData;

    public int TurnsRemaining
    {
        get
        {
            int turns = jobData.Turns - gameController.TurnController.TurnNumber;

            if (turns < 0)
                turns = 0;

            return turns;
        }
    }

    public override void ControllerInit()
    {
        D.Trace("[JobController] ControllerInit");
        sessionData = gameController.SessionData;
        jobData = jobBoards.Jobs[sessionData.Job];
        StartCoroutine(loadScene(jobData.Scene));
    }

    public override void ControllerStart()
    {
        gameController.VisitorController.Visitors = jobData.Visitors;
    }

    public void NextJob()
    {
        sessionData.Job += 1;

        if (sessionData.Job >= jobBoards.Jobs.Length)
        {
            SceneManager.LoadScene("win");
            return;
        }

        SceneManager.LoadScene("game");
    }

    private IEnumerator loadScene(string name)
    {
        AsyncOperation async = SceneManager.LoadSceneAsync(name, LoadSceneMode.Additive);
        while(!async.isDone)
        {
            D.Trace(".");
            yield return null;
        }
        SimpleEvents.Instance.Raise(new SceneLoadedEvent());
    }

    public override void OnEnable()
    {
        base.OnEnable();
        gameController = GameObject.FindObjectOfType<GameController>();
    }
}
