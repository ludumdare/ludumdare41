﻿using Sdn.Core.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RoomController : Controller
{
    public GameObject DebugObject;

    public FloorTileBrush[] floorTileBrushes { get; set; }
    public FloorTilePassableBrush[] floorTilePassableBrushes { get; set; }

    private Dictionary<Vector2, FloorTile> floorTiles;

    public FloorTileBrush[] GetFloorTiles()
    {
        return floorTileBrushes;
    }

    public override void ControllerInit()
    {
        //  initialize all room brushes

        Brush[] brushes = GameObject.FindObjectsOfType<Brush>();

        foreach(Brush brush in brushes)
        {
            brush.BrushInit();
        }

        initFloorTiles();

    }

    public override void ControllerStart()
    {
        base.ControllerStart();
    }

    public FloorTile GetTile(Vector2 pos)
    {
        Vector2 key = pos;
        if (floorTiles.ContainsKey(key))
        {
            return floorTiles[key];
        }

        return null;
    }

    //  return tiles in a range around position
    public IEnumerable<FloorTile> GetTilesInRage(Vector2 pos, int range)
    {
        if (range == 0)
        {
            FloorTile tile = GetTile(pos);

            if (tile != null)
                yield return tile;

            yield break;
        }

        for (int x = -range; x < range + 1; x++)
        {
            for (int y = -range; y < range + 1; y++)
            {
                Vector2 tilePos = pos + (new Vector2(x, y) * 30.0f);
                FloorTile tile = GetTile(tilePos);
                //Instantiate(DebugObject, tilePos, Quaternion.identity);
                //D.Trace("- x: {0}, y: {1}, floorTile: {2}", x, y, tile);
                if (tile != null)
                    yield return tile;
            }
        }
    }

    private void initFloorTiles()
    {
        D.Trace("[RoomController] initFloorTiles");
        D.Fine("- creating floor tiles");
        floorTiles = new Dictionary<Vector2, FloorTile>();


        floorTileBrushes = GameObject.FindObjectsOfType<FloorTileBrush>();

        foreach(FloorTileBrush brush in floorTileBrushes)
        {
            D.Fine("- child: " + brush.gameObject.name);

            GameObject go = brush.gameObject;
            FloorTile f = new FloorTile();
            f.IsOccupied = false;
            f.IsWalkable = true;
            f.IsPassable = true;
            f.FloorTileObject = go;
            f.X = Mathf.FloorToInt(go.transform.position.x);
            f.Y = Mathf.FloorToInt(go.transform.position.y);

            D.Fine("- X: {0}, Y: {1}", f.X, f.Y);

            Vector2 key = new Vector2(f.X, f.Y);
            if (!floorTiles.ContainsKey(key))
                floorTiles.Add(key, f);

        }

        floorTilePassableBrushes = GameObject.FindObjectsOfType<FloorTilePassableBrush>();

        foreach (FloorTilePassableBrush brush in floorTilePassableBrushes)
        {
            D.Fine("- child: " + brush.gameObject.name);

            GameObject go = brush.gameObject;
            FloorTile f = new FloorTile();
            f.IsOccupied = false;
            f.IsPassable = true;
            f.IsWalkable = false;
            f.FloorTileObject = go;
            f.X = Mathf.FloorToInt(go.transform.position.x);
            f.Y = Mathf.FloorToInt(go.transform.position.y);

            D.Fine("- X: {0}, Y: {1}", f.X, f.Y);

            Vector2 key = new Vector2(f.X, f.Y);
            if (!floorTiles.ContainsKey(key))
                floorTiles.Add(key, f);

        }

    }
}

public class FloorTile
{
    public int X { get; set; }
    public int Y { get; set; }
    public bool IsWalkable { get; set; }
    public bool IsOccupied { get; set; }
    public bool IsPassable { get; set; }

    public GameObject FloorTileObject { get; set; }
    public Moveable OccupiedObject { get; set; }

    public Vector2 GetWorldPosition()
    {
        Vector2 pos = new Vector2(X, Y);
        return pos;
    }

}
