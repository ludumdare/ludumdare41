﻿using UnityEngine;
using System.Collections.Generic;
using Sdn.Core.Controllers;

public class ScareController : Controller
{
    public Scare[] scares;
    public Dictionary<string, Scare> keyedScares;

    public Scare CreateScare(string name)
    {
        D.Trace("[ScareController] CreateScare");

        if (keyedScares.ContainsKey(name.ToLower()))
        {
            GameObject go = Instantiate(keyedScares[name.ToLower()].gameObject);
            return go.GetComponent<Scare>();
        }

        D.Warn("- Scare {0} does not exist", name);
        return null;
    }

    public override void ControllerInit()
    {
        keyedScares = new Dictionary<string, Scare>();
        foreach(Scare scare in scares)
        {
            keyedScares.Add(scare.name.ToLower(), scare);
        }
    }


}
