﻿using Sdn.Core.Controllers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CameraController : Controller
{
    public float delay = 5.0f;

    private Transform target;
    private Camera camera;

    public override void ControllerInit()
    {
        D.Trace("[CameraController] ControllerInit");
        target = GameObject.FindObjectOfType<Player>().transform;
        camera = Camera.main;
    }

    public override void ControllerStart()
    {
        isActive = true;
    }

    private void Update()
    {
        if (!IsActive)
            return;

        float x = Mathf.Lerp(target.position.x, camera.transform.position.x, Time.deltaTime * delay);
        float y = Mathf.Lerp(target.position.y, camera.transform.position.y, Time.deltaTime * delay);
        Vector3 pos = new Vector3(0, target.position.y, 0);

        camera.transform.position = pos;
    }
}
