﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Controllers;

public class VisitorController : TurnableController
{
    public int Waves;
    public int Visitors;
    public int SpawnRate;
    public Visitor visitor;

    private VisitorSpawner[] spawners;
    private int totalVisitors;
    private int totalTurns = 9999;

    public int VisitorsRemaining { get; set; }

    public override void TurnableInit()
    {
        D.Trace("[VisitorController] TurnableInit");
        spawners = GameObject.FindObjectsOfType<VisitorSpawner>();
        VisitorsRemaining = Visitors;
    }

    public override IEnumerator TurnableTurn()
    {
        if (totalVisitors >= Visitors)
        {
            yield break;
        }

        totalTurns += 1;

        if (totalTurns >= SpawnRate)
        {
            spawnVisitor();
            totalTurns = 0;
        }

        yield return null;
    }

    private void spawnVisitor()
    {
        totalVisitors += 1;
        VisitorsRemaining = Visitors - totalVisitors;
        GameObject spawner = (GameObject)spawners[0].gameObject;
        GameObject go = Instantiate(visitor.gameObject, Vector2.zero , Quaternion.identity);
        go.GetComponent<Visitor>().GameController = GameController;
        go.GetComponent<Visitor>().MoveTo(spawner.transform.position);
        GameController.TurnController.AddTurnable(go.GetComponent<Visitor>());
    }
}
