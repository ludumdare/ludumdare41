﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTileBrush : Brush
{
    public override void BrushInit()
    {
        D.Trace("[FloorTileBrush] BrushInit");
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
