﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FloorTilePassableBrush : Brush
{
    public override void BrushInit()
    {
        D.Trace("[FloorTilePassableBrush] BrushInit");
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
