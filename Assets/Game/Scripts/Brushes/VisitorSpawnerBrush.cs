﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class VisitorSpawnerBrush : Brush
{
    public override void BrushInit()
    {
        D.Trace("[VisitorSpawnerBrush] BrushInit");
        GetComponent<SpriteRenderer>().enabled = false;
    }
}
