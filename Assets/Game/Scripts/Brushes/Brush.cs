﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Brush : MonoBehaviour
{

    public FloorTile FloorTile { get; set; }

    public virtual void BrushInit()
    {
        D.Trace("[Brush] BrushInit");
    }

    public virtual void BrushStart()
    {
        D.Trace("[Brush] BrushStart");
    }

    public virtual void BrushStop()
    {
        D.Trace("[Brush] BrushStop");
    }

}
