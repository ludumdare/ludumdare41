﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerStartBrush : Brush
{
    public override void BrushInit()
    {
        D.Trace("[PlayerStartBrush] BrushInit");
        GetComponent<SpriteRenderer>().enabled = false;
        GameObject.FindObjectOfType<Player>().transform.position = transform.position;
    }
}
