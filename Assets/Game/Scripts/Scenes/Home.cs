﻿using Sdn.Core.Game;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Home : BaseScene
{
    public void OpenScene(string name)
    {
        gameManager.GotoScene(name);
    }

    public void QuitGame()
    {
        Application.Quit();
    }

    private void Start()
    {
        GameSettingsData settings = (GameSettingsData)gameManager.Settings;
        settings.SessionData1.Job = 0;
        settings.SessionData1.Score = 0;
        settings.SessionData1.Lives = 0;
        settings.SessionData1.Lives = 0;
        MusicManager.Instance.PlayMusic("title", 0.5f);
    }

}
