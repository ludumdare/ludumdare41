﻿using Sdn.Core;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class Game : BaseScene
{
    public Button PrimaryButton;
    public Button SecondaryButton;
    public Button AltButton1;
    public Button AltButton2;

    private GameController gameController;
    private JobController jobController;

    bool active;


    public void PrimaryScare()
    {
    }

    public void SecondaryScare()
    {
    }

    public void AltScare1()
    {
    }

    public void AltScare2()
    {
    }

    private void Update()
    {
        if (!active)
            return;

        PrimaryButton.transform.Find("Image").GetComponent<Image>().enabled = false;
        SecondaryButton.transform.Find("Image").GetComponent<Image>().enabled = false;
        AltButton1.transform.Find("Image").GetComponent<Image>().enabled = false;
        AltButton2.transform.Find("Image").GetComponent<Image>().enabled = false;

        if (gameController.PrimaryScare.IsReady)
        {
            PrimaryButton.transform.Find("Image").GetComponent<Image>().enabled = true;
        }
        if (gameController.SecondaryScare.IsReady)
        {
            SecondaryButton.transform.Find("Image").GetComponent<Image>().enabled = true;
        }
        if (gameController.AltScare1.IsReady)
        {
            AltButton1.transform.Find("Image").GetComponent<Image>().enabled = true;
        }
        if (gameController.AltScare2.IsReady)
        {
            AltButton2.transform.Find("Image").GetComponent<Image>().enabled = true;
        }
    }

    private void Start()
    {
        MusicManager.Instance.PlayMusic("game", 0.5f);
        gameController = GameObject.FindObjectOfType<GameController>();
        jobController = GameObject.FindObjectOfType<JobController>();
        jobController.ControllerInit();
    }

    private void sceneLoadedListener(SceneLoadedEvent e)
    {
        GameObject.Find("Ground").gameObject.SetActive(false);
        gameController.ControllerInit();

        PrimaryButton.transform.Find("Image").GetComponent<Image>().sprite = gameController.PrimaryScare.GetComponent<SpriteRenderer>().sprite;
        SecondaryButton.transform.Find("Image").GetComponent<Image>().sprite = gameController.SecondaryScare.GetComponent<SpriteRenderer>().sprite;
        AltButton1.transform.Find("Image").GetComponent<Image>().sprite = gameController.AltScare1.GetComponent<SpriteRenderer>().sprite;
        AltButton2.transform.Find("Image").GetComponent<Image>().sprite = gameController.AltScare2.GetComponent<SpriteRenderer>().sprite;
        gameController.ControllerStart();
        active = true;
    }

    public override void OnEnable()
    {
        base.OnEnable();
        SimpleEvents.Instance.AddListener<SceneLoadedEvent>(sceneLoadedListener);
    }

    private void OnDisable()
    {
        SimpleEvents.Instance.RemoveListener<SceneLoadedEvent>(sceneLoadedListener);
    }
}
