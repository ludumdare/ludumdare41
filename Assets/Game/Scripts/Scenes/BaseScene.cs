﻿using Sdn.Core.Data;
using Sdn.Core.Game;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class BaseScene : Scene
{
    private GameData gameData;
    private GameSettingsData settingsData;

    public override void OnEnable()
    {
        base.OnEnable();
        gameData = (GameData)gameManager.Data;
        settingsData = (GameSettingsData)gameManager.Settings;
    }
}
