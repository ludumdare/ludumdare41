﻿using Sdn.Core.Data;
using Sdn.Core.Game;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class TheBootstrap : Bootstrap
{
    public GameSettingsData settings;
    private GameData data;
    public override void OnEnable()
    {
        base.OnEnable();
        data = (GameData)gameManager.Data;
        gameManager.LoadSettings(settings);
        ScreenManager.Instance.SetToDefaultView();
        gameManager.GotoScene(data.HomeScene);
        
    }
}
