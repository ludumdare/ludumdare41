﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
public class EffectSpriteData : ScriptableObject
{
    public Sprite[] EffectSprites;
}
