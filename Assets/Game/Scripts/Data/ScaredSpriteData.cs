﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
public class ScaredSpriteData : ScriptableObject
{
    public Sprite[] ScaredSprites;
}
