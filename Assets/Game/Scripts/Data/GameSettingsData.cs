﻿using Sdn.Core.Data;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

[CreateAssetMenu]
public class GameSettingsData : SettingsData
{
    public KeyCode KeyUp;
    public KeyCode KeyDown;
    public KeyCode KeyLeft;
    public KeyCode KeyRight;
    public KeyCode KeyActionPrimary;
    public KeyCode KeyActionSecondary;
    public KeyCode KeyActionAlternate1;
    public KeyCode KeyActionAlternate2;
    public KeyCode KeyActionAlternate3;
    public KeyCode KeyActionAlternate4;
    public GameSessionData SessionData1;
    public GameSessionData SessionData2;
    public GameSessionData SessionData3;
}
