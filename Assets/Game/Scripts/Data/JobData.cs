﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

[CreateAssetMenu]
[System.Serializable]
public class JobData : ScriptableObject
{
    public string Name;
    public string Scene;
    public int Waves;
    public int Visitors;
    public int Turns;
    public int Bonus;
}
