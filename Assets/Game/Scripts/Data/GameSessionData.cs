﻿using Sdn.Core.Data;
using System;
using UnityEngine;

[CreateAssetMenu]
[System.Serializable]
public class GameSessionData : SessionData
{
    //  current level the player is on
    public int Level;

    //  current job the player is on (if needed)
    public int Job;

    //  current wave the player is on (if needed)
    public int Wave;

    //  available lives
    public int Lives;

    //  current score
    public int Score;

    //  best score
    public int BestScore;

    //  available power
    public int Power;

    //  max power
    public int MaxPower;

    //  cash on hand
    public int Cash;

    public string PrimaryScare;
    public string SecondaryScare;
    public string AltScare1;
    public string AltScare2;

    public GameSessionData()
    {
        Id = Guid.NewGuid();
    }
}
