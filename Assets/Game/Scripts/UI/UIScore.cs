﻿using UnityEngine;
using System.Collections;
using UnityEngine.UI;
using Sdn.Core;

/// <summary>
/// Continually Updates the Score on the attached Text object.
/// </summary>
public class UIScore : MonoBehaviour
{
    private Text scoreText;
    private float score;
    private float nextScore;

    private void scoreChangedListener(ScoreChangedEvent e)
    {
        score = e.Score;
    }

    private void Update()
    {
        D.Fine("- score: {0}, nextScore: {1}", score, nextScore);
        if (score-nextScore > 10)
        {
            nextScore = Mathf.Lerp(nextScore, score, 0.15f);
        } else
        {
            if (nextScore < score)
            {
                nextScore += 1;
            }
        }
        scoreText.text = string.Format("Score: {0:0}", nextScore);
    }

    private void OnEnable()
    {
        scoreText = GetComponent<Text>();
        SimpleEvents.Instance.AddListener<ScoreChangedEvent>(scoreChangedListener);
    }

    private void OnDisable()
    {
        SimpleEvents.Instance.RemoveListener<ScoreChangedEvent>(scoreChangedListener);
    }
}
