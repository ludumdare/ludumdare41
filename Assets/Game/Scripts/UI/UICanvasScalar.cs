﻿using Sdn.Core;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UICanvasScalar : MonoBehaviour
{
    private ScreenView view;
    private CanvasScaler scalar;

    private void screenViewChanged(ScreenViewChangedEvent e)
    {
        view = e.CurrentView;
        scalar.scaleFactor = view.CanvasScale;
    }

    private void OnDisable()
    {
        SimpleEvents.Instance.RemoveListener<ScreenViewChangedEvent>(screenViewChanged);
    }

    private void OnEnable()
    {
        SimpleEvents.Instance.AddListener<ScreenViewChangedEvent>(screenViewChanged);
        scalar = GetComponent<CanvasScaler>();
        view = ScreenManager.Instance.CurrentView();
        scalar.scaleFactor = view.CanvasScale;
    }
}
