﻿using Sdn.Core;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScale : MonoBehaviour
{
    public Text scaleText;
    private ScreenView view;

    private void screenViewChanged(ScreenViewChangedEvent e)
    {
        view = e.CurrentView;
        scaleText.text = view.Name;
    }

    private void OnDisable()
    {
        SimpleEvents.Instance.RemoveListener<ScreenViewChangedEvent>(screenViewChanged);
    }

    private void OnEnable()
    {
        view = ScreenManager.Instance.CurrentView();
        scaleText.text = view.Name;
        SimpleEvents.Instance.AddListener<ScreenViewChangedEvent>(screenViewChanged);
    }
}
