﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.UI;

public class UIScene : MonoBehaviour
{
    private Text sceneName;

    private void OnEnable()
    {
        sceneName = GetComponent<Text>();
        sceneName.text = SceneManager.GetActiveScene().name;
    }
}
