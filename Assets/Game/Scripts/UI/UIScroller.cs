﻿using Sdn.Core.Data;
using Sdn.Core.Managers;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UIScroller : MonoBehaviour
{
    public float Speed;
    public float ScrollTime;

    private Text scrollText;
    private GameData gameData;
    private Vector3 startPosition;

    private void OnEnable()
    {
        transform.position = transform.position + (Vector3.right * Screen.width);
        startPosition = transform.position;
        scrollText = GetComponent<Text>();
        gameData = (GameData)GameManager.Instance.Data;
        scrollText.text = gameData.Message;
        StartCoroutine(scroller());
        StartCoroutine(timer());
    }

    private IEnumerator scroller()
    {
        while(true)
        {
            transform.Translate(Vector3.left * Speed * Time.deltaTime);
            yield return null;
        }
    }

    private IEnumerator timer()
    {
        while (true)
        {
            yield return new WaitForSeconds(ScrollTime);
            transform.position = startPosition;
        }
    }

}
