﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UITurns : MonoBehaviour
{
    private Text text;
    private GameController gameController;

    private void Update()
    {
        if (gameController == null)
            gameController = GameObject.FindObjectOfType<GameController>();

        if (gameController != null)
        {
            if (gameController.JobController != null)
            {
                text.text = "Turn:" + gameController.JobController.TurnsRemaining.ToString();
            }
        }
    }

    private void OnEnable()
    {
        text = GetComponent<Text>();
    }
}
