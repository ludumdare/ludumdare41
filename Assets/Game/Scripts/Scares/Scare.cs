﻿using UnityEngine;
using System.Collections;
using Sdn.Core.Managers;

public class Scare : Effect
{
    public int Cooldown;    //  length of time for cooldown
    public int Duration;    //  length of the scare in turns
    public int Strength;    //  strength of the scare
    public int Points;      //  points for successful scare
    public int Range;       //  range of scare
    public string Sound;    //  sound to play on scare

    public bool IsReady { get; set; }
    public bool IsRunning { get;set; }

    SpriteRenderer overlay;

    [SerializeField]
    private int durationTurns;
    [SerializeField]
    private int cooldownTurns;

    //  initialize the scare
    public virtual void ScareInit()
    {
        D.Trace("[Scare] ScareInit");
        IsReady = true;
        IsRunning = false;
        IsActive = false;
        hide();
    }

    public virtual void ScareBeforStart()
    {
        D.Trace("[Scare] ScareBeforeStart");
    }

    public virtual void ScareStart()
    {
        D.Trace("[Scare] ScareStart");
        IsReady = false;
        IsRunning = true;
        IsActive = true;
        EffectInit();
        EffectStart();
    }

    public virtual void ScareAfterStart()
    {
        D.Trace("[Scare] ScareAfterStart");
    }

    public virtual void ScareStop()
    {
        D.Trace("[Scare] ScareStop");
        EffectStop();
    }

    public override IEnumerator TurnableTurn()
    {
        D.Trace("[Scare] TurnableTurn");
        
        //  see if a visitor is affected by the scare
        foreach(FloorTile tile in GameController.RoomController.GetTilesInRage(transform.position, Range))
        {
            D.Fine("- tile: {0}, isOccupied: {1}", tile, tile.IsOccupied);
            if (tile.IsOccupied)
            {
                Actor actor = tile.OccupiedObject.GetComponent<Actor>();
                if (actor != null)
                {
                    if (actor.ActorType == Actor.ActorTypes.ACTOR_VISITOR)
                    {
                        actor.Scare(this);
                        SoundManager.Instance.PlaySound(Sound);
                    }
                }
            }
        }
        yield return null;
    }

    public override IEnumerator TurnableAfterTurn()
    {
        D.Trace("[Scare] TurnableAfterTurn");

        if (!IsReady)
        {
            cooldownTurns += 1;

            if (cooldownTurns > Cooldown)
            {
                IsReady = true;
                IsActive = false;
                cooldownTurns = 0;
            }
        }

        if (!IsRunning)
            yield break;

        durationTurns += 1;

        if (durationTurns > Duration)
        {
            durationTurns = 0;
            IsRunning = false;
            hide();
        }

    }

    private void hide()
    {
        transform.position = new Vector2(-10000, -10000);
    }

    private void OnEnable()
    {
        overlay = transform.Find("Overlay").GetComponent<SpriteRenderer>();
        if (Range > 0)
            overlay.gameObject.transform.localScale = new Vector3(Range + 2.25f, Range + 2.25f, 1.0f);

    }

}
