﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// This Behavior will move a Visitor in a Random Direction
/// it will stop every 5-10 turns, and then pick another
/// direction.
/// </summary>
/// <remarks>
/// This behavior will pick another direction if the
/// Visitor has been put into an Idle State
/// </remarks>
public class DefaultBehavior : Behavior
{
    private int nextTurn;
    private int totalTurns;

    public override void BehaviorInit()
    {
        D.Trace("[DefaultBehavior] BehaviorInit");
        getNextTurn();
        Visitor.Direction = Visitor.GetNextDirection();
    }

    public override void BehaviorRun()
    {
        D.Trace("[DefaultBehavior] BehaviorRun");

        if (Visitor.State == Visitor.VisitorStates.STATE_SCARED)
            return;

        totalTurns += 1;

        if (totalTurns > nextTurn || Visitor.State == Visitor.VisitorStates.STATE_IDLE || Visitor.State == Visitor.VisitorStates.STATE_CONFUSED)
        {
            Visitor.Direction = Visitor.GetNextDirection();
            D.Trace("- Direction: {0}", Visitor.Direction);
            Visitor.ChangeState(Visitor.VisitorStates.STATE_MOVING);
            getNextTurn();
        }
    }

    private void getNextTurn()
    {
        totalTurns = 0;
        nextTurn = Random.Range(5, 10);
    }
}
