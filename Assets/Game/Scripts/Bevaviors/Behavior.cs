﻿using UnityEngine;
using System.Collections;

public class Behavior
{
    public Visitor Visitor { get; set; }

    public virtual void BehaviorInit()
    {
        D.Trace("[BehaviorInit] BehaviorInit");
    }

    public virtual void BehaviorRun()
    {
        D.Trace("[BehaviorInit] BehaviorRun");
    }
}
