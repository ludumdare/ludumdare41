﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

/// <summary>
/// What to do when the Visitor becomes Scared 
/// 
/// Which is to wait three turns and then continue
/// 
/// </summary>
public class ScaredBehavior : Behavior
{
    private int turns;
    public override void BehaviorInit()
    {
        base.BehaviorInit();
    }

    public override void BehaviorRun()
    {
        if (Visitor.State == Visitor.VisitorStates.STATE_SCARED)
        {
            turns += 1;

            if (turns > 2)
            {
                Visitor.ChangeState(Visitor.VisitorStates.STATE_IDLE);
                turns = 0;
            }
        }
    }
}
