﻿using UnityEngine;
using System.Collections;

public class Turnable : MonoBehaviour
{
    public GameController GameController { get; set; }
    public int TurnOrder { get; set; }
    public bool IsActive { get; set; }
    

    //  reset the turnable properties
    public virtual void TurnableReset()
    {
        D.Trace("[Turnable] TurnableReset");
    }

    //  initialize the turnable
    public virtual void TurnableInit()
    {
        D.Trace("[Turnable] TurnableInit");
    }

    //  something to do just before the turn
    public virtual IEnumerator TurnableBeforeTurn()
    {
        D.Trace("[Turnable] TurnableBeforeTurn");
        yield return null;
    }

    // the actual turn itself
    public virtual IEnumerator TurnableTurn()
    {
        D.Trace("[Turnable] TurnableTurn");
        yield return null;
    }

    //  something to do after the turn
    public virtual IEnumerator TurnableAfterTurn()
    {
        D.Trace("[Turnable] TurnableAfterTurn");
        yield return null;
    }

    private void OnEnable()
    {
        IsActive = true;
    }


}
