﻿using UnityEngine;
using System.Collections;

public class Actor : Moveable
{
    public enum ActorTypes
    {
        ACTOR_PLAYER,
        ACTOR_VISITOR
    }

    public ActorTypes ActorType { get; set; }

    public Vector2 GetNextDirection()
    {
        D.Trace("[Visitor] getNextDirection");
        ArrayList availableMoves = new ArrayList();

        if (CanMove(Vector2.up))
            availableMoves.Add(Vector2.up);

        if (CanMove(Vector2.down))
            availableMoves.Add(Vector2.down);

        if (CanMove(Vector2.left))
            availableMoves.Add(Vector2.left);

        if (CanMove(Vector2.right))
            availableMoves.Add(Vector2.right);

        D.Fine("- availableMoves: {0}", availableMoves.Count);

        if (availableMoves.Count > 0)
        {
            int r = Random.Range(0, availableMoves.Count);
            D.Fine("- r: {0}, dir:", r, availableMoves[r]);
            return (Vector2)availableMoves[r];
        }

        return Vector2.zero;
    }

    public virtual void Scare(Scare scare)
    {
        D.Trace("[Visitor] Scare");
    }

}
