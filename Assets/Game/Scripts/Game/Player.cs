﻿using UnityEngine;
using System.Collections;

public class Player : Actor
{
    private Scare primaryScare;
    private Scare secondaryScare;
    private Scare altScare1;
    private Scare altScare2;

    public override void TurnableInit()
    {
        base.TurnableInit();
        TurnOrder = -1;
        primaryScare = GameController.PrimaryScare;
        //GameController.TurnController.AddTurnable(primaryScare);
        secondaryScare = GameController.SecondaryScare;
        //GameController.TurnController.AddTurnable(secondaryScare);
        altScare1 = GameController.AltScare1;
        //GameController.TurnController.AddTurnable(altScare1);
        altScare2 = GameController.AltScare2;
        //GameController.TurnController.AddTurnable(altScare2);
        ActorType = ActorTypes.ACTOR_PLAYER;
    }

    public override IEnumerator TurnableTurn()
    {
        D.Trace("[Player] TurnableTurn");

        while(true)
        {
            yield return null;

            if (Input.GetKey(GameController.SettingsData.KeyLeft))
            {
                if (CanMove(Vector2.left))
                {
                    yield return StartCoroutine(MoveLeft());
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyRight))
            {
                if (CanMove(Vector2.right))
                {
                    yield return StartCoroutine(MoveRight());
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyUp))
            {
                if (CanMove(Vector2.up))
                {
                    yield return StartCoroutine(MoveUp());
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyDown))
            {
                if (CanMove(Vector2.down))
                {
                    yield return StartCoroutine(MoveDown());
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyActionPrimary))
            {
                if (primaryScare.IsReady)
                {
                    activateScare(primaryScare);
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyActionSecondary))
            {
                if (secondaryScare.IsReady)
                {
                    activateScare(secondaryScare);
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyActionAlternate1))
            {
                if (altScare1.IsReady)
                {
                    activateScare(altScare1);
                    yield break;
                }
            }

            if (Input.GetKey(GameController.SettingsData.KeyActionAlternate2))
            {
                if (altScare2.IsReady)
                {
                    activateScare(altScare2);
                    yield break;
                }
            }

            yield return null;

        }

    }

    public override bool CanMove(Vector2 moveAction)
    {
        D.Trace("[Moveable] CanMove");

        bool move = true;

        Vector2 newpos = (Vector2)transform.position + (moveAction * 30.0f);

        FloorTile floorTile = GameController.RoomController.GetTile(newpos);
        D.Fine("- floorTile: {0}", floorTile);

        if (floorTile == null)
        {
            move = false;
        }

        return move;
    }
    private void activateScare(Scare scare)
    {
        scare.transform.position = transform.position;
        scare.ScareBeforStart();
        scare.ScareStart();
        scare.ScareAfterStart();
    }
}
