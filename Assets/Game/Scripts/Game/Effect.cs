﻿using UnityEngine;
using System.Collections;

public class Effect : Turnable
{
    public virtual void EffectInit()
    {
        D.Trace("[Effect] EffectInit");
    }

    public virtual void EffectStart()
    {
        D.Trace("[Effect] EffectStart");
    }

    public virtual void EffectStop()
    {
        D.Trace("[Effect] EffectStop");
    }

}
