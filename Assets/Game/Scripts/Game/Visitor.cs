﻿using UnityEngine;
using System.Collections.Generic;
using System.Collections;
using Sdn.Core;
using DG.Tweening;

public class Visitor : Actor
{
    public enum VisitorStates
    {
        STATE_IDLE,
        STATE_SCARED,
        STATE_MOVING,
        STATE_SEARCHING,
        STATE_LISTENING,
        STATE_CONFUSED,
        STATE_TOUCHING,
        STATE_ESCAPING
    }

    public EffectSpriteData effectSpriteData;
    public ScaredSpriteData scaredSpriteData;

    public SpriteRenderer effectRenderer;
    public SpriteRenderer scareRenderer;

    private List<Behavior> behaviors;

    private DefaultBehavior defaultBehavior;
    private ScaredBehavior scaredBehavior;

    public int Bravery { get; set; }
    public VisitorStates State { get; set; }
    public Vector2 Direction { get; set; }

    public void ChangeState(VisitorStates state)
    {
        changeState(state);
    }

    public override void TurnableInit()
    {
        D.Trace("[Visitor] TurnableInit");
        base.TurnableInit();
        ActorType = ActorTypes.ACTOR_VISITOR;
        TurnOrder = 99;
        changeState(VisitorStates.STATE_IDLE);

        foreach(Behavior behavior in behaviors)
        {
            behavior.BehaviorInit();
        }
    }

    public override IEnumerator TurnableBeforeTurn()
    {
        yield return null;
    }

    public override IEnumerator TurnableTurn()
    {
        D.Trace("[Visitor] TurnableTurn");
        D.Trace("- State Before: {0}", State);

        //  run behaviors and then check states

        foreach (Behavior behavior in behaviors)
        {
            behavior.BehaviorRun();
        }

        D.Trace("- State After: {0}", State);

        //  still idle - do nothing
        if (State == VisitorStates.STATE_IDLE)
        {
            yield break;
        }

        //  moving - try moving to the next position
        if (State == VisitorStates.STATE_MOVING)
        {
            if (CanMove(Direction))
            {
                yield return StartCoroutine(Move(Direction));
            } else
            {
                changeState(VisitorStates.STATE_CONFUSED);
            }
        }
        yield return null;
    }

    public override IEnumerator TurnableAfterTurn()
    {
        D.Trace("[Visitor] TurnableAfterTurn");

        if (Bravery > 0)
            scareRenderer.sprite = scaredSpriteData.ScaredSprites[Bravery - 1];

        yield return null;
    }

    public override void Scare(Scare scare)
    {
        //  already scared?
        if (State == VisitorStates.STATE_SCARED)
            return;

        Bravery -= scare.Strength;

        changeState(VisitorStates.STATE_SCARED);
        GameController.SessionData.Score += scare.Points;

        SimpleEvents.Instance.Raise(new ScoreChangedEvent
        {
            Score = GameController.SessionData.Score
        });

        if (Bravery <= 0)
        {
            changeState(VisitorStates.STATE_ESCAPING);
            IsActive = false;
            escapeRoom();
        }

    }

    private void escapeRoom()
    {
        transform.DOMoveY(transform.position.y + 1000, 4.0f);
        CurrentTile.IsOccupied = false;
    }

    private void changeState(VisitorStates state)
    {
        effectRenderer.enabled = true;
        State = state;

        if (state == VisitorStates.STATE_IDLE)
            effectRenderer.enabled = false;

        if (state == VisitorStates.STATE_ESCAPING)
            effectRenderer.enabled = false;

        if (state == VisitorStates.STATE_SCARED)
            effectRenderer.sprite = effectSpriteData.EffectSprites[0];

        if (state == VisitorStates.STATE_CONFUSED)
            effectRenderer.sprite = effectSpriteData.EffectSprites[1];

        if (state == VisitorStates.STATE_SEARCHING)
            effectRenderer.sprite = effectSpriteData.EffectSprites[2];

        if (state == VisitorStates.STATE_LISTENING)
            effectRenderer.sprite = effectSpriteData.EffectSprites[3];

        if (state == VisitorStates.STATE_TOUCHING)
            effectRenderer.sprite = effectSpriteData.EffectSprites[4];

        if (state == VisitorStates.STATE_MOVING)
            effectRenderer.sprite = effectSpriteData.EffectSprites[5];

    }

    private void OnEnable()
    {
        behaviors = new List<Behavior>();
        defaultBehavior = new DefaultBehavior();
        defaultBehavior.Visitor = this;
        behaviors.Add(defaultBehavior);
        scaredBehavior = new ScaredBehavior();
        scaredBehavior.Visitor = this;
        behaviors.Add(scaredBehavior);
        IsActive = true;
        effectRenderer.enabled = false;
        //effectScares.enabled = false;
        changeState(VisitorStates.STATE_IDLE);
        Bravery = Random.Range(3, 6);
        scareRenderer.sprite = scaredSpriteData.ScaredSprites[Bravery - 1];
    }
}
