﻿using UnityEngine;
using System.Collections;

public class Moveable : Turnable
{
    public float Speed;

    private float facingLeft = 1;
    private float tileSize = 30.0f;

    public FloorTile CurrentTile { get; set; }
    public FloorTile PreviousTile { get; set; }

    public virtual IEnumerator MoveLeft()
    {
        D.Trace("[Moveable] MoveLeft");
        facingLeft = 1.0f;
        yield return StartCoroutine(move(Vector2.left));
    }

    public virtual IEnumerator MoveRight()
    {
        D.Trace("[Moveable] MoveRight");
        facingLeft = -1.0f;
        yield return StartCoroutine(move(Vector2.right));
    }

    public virtual IEnumerator MoveUp()
    {
        D.Trace("[Moveable] MoveUp");
        yield return StartCoroutine(move(Vector2.up));
    }

    public virtual IEnumerator MoveDown()
    {
        D.Trace("[Moveable] MoveDown");
        yield return StartCoroutine(move(Vector2.down));
    }

    public virtual IEnumerator Move(Vector2 dir)
    {
        D.Trace("[Moveable] Move");
        yield return StartCoroutine(move(dir));
    }

    public virtual void MoveTo(Vector2 pos)
    {
        D.Trace("[Moveable] MoveTo");
        transform.position = pos;
        setTiles(pos);
    }

    private IEnumerator move(Vector2 dir)
    {
        D.Trace("[Moveable] move");
        transform.localScale = new Vector2(facingLeft, 1);
        Vector2 newPosition = (Vector2)transform.position + (Vector2)(dir * 30.0f);
        setTiles(newPosition);
        yield return StartCoroutine(moveTo(newPosition));
    }

    private void setTiles(Vector2 pos)
    {
        D.Trace("[Moveable] setTiles");
        FloorTile floorTile = GameController.RoomController.GetTile(pos);

        if (CurrentTile != null)
        {
            PreviousTile = CurrentTile;
            PreviousTile.IsOccupied = false;
            PreviousTile.OccupiedObject = null;
            PreviousTile.IsWalkable = true;
        }

        if (floorTile != null)
        {
            CurrentTile = floorTile;
            CurrentTile.IsOccupied = true;
            CurrentTile.OccupiedObject = this;
            CurrentTile.IsWalkable = false;
        }

    }

    private IEnumerator moveTo(Vector2 pos)
    {
        D.Trace("[Moveable] moveTo");
        D.Fine("- moving to pos {0} on scene", pos);
        float dist = Vector2.Distance(transform.position, pos);
        //  as long as we are active, keep moving until we reach our destination
        while (dist > 0)
        {
            Vector2 newpos = Vector2.MoveTowards(transform.position, pos, Speed * Time.deltaTime);
            transform.position = newpos;
            dist = Vector2.Distance(transform.position, pos);
            D.Fine("- dist is {0}", dist);
            yield return null;
        }
        transform.position = pos;
    }

    public void Action()
    {
        D.Trace("[Moveable] Action");
    }

    public virtual bool CanMove(Vector2 moveAction)
    {
        D.Trace("[Moveable] CanMove");

        bool move = true;

        Vector2 newpos = (Vector2)transform.position + (moveAction * 30.0f);

        FloorTile floorTile = GameController.RoomController.GetTile(newpos);
        D.Fine("- floorTile: {0}", floorTile);

        if (floorTile == null)
        {
            move = false;
        } else
        {
            if (floorTile.IsOccupied)
            {
                move = false;
            }

            if (!floorTile.IsWalkable)
            {
                move = false;
            }
        }

        return move;
    }
}
