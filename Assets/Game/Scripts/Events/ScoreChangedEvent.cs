﻿using Sdn.Core;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreChangedEvent : SimpleEvent
{
    [SerializeField]
    public int Score;
}
