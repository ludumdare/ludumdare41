﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Tilemaps;

public class WorkTilemap : MonoBehaviour
{
    public GameController gameController;


    private void Start()
    {
        gameController.ControllerInit();
        gameController.ControllerStart();

        //StartCoroutine(runPlayer());
    }

    private IEnumerator runPlayer()
    {
        while(true)
        {
            yield return StartCoroutine(gameController.PlayerController.Player.TurnableTurn());
            yield return null;
        }
    }
}
