﻿using Sdn.Core;
using Sdn.Core.Data;
using Sdn.Core.Game;
using Sdn.Core.Managers;
using Sdn.SpriteBoss;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class WorkCamera : Bootstrap
{
    private GameData gameData;
    public CanvasScaler scalar;
    private ScreenView view;

    public Text size;

    private void Update()
    {
    }

    private void screenViewChanged(ScreenViewChangedEvent e)
    {
        view = e.CurrentView;
        size.text = view.Name;
        //scalar.scaleFactor = view.CanvasScale;
    }

    private void LateUpdate()
    {
        //spritebossCanvas.GetComponent<RectTransform>().sizeDelta = new Vector2(view.Width, view.Height);
    }

    private void Start()
    {
        ScreenManager.Instance.SetToDefaultView();
    }

    public override void OnEnable()
    {
        base.OnEnable();

        gameData = (GameData)gameManager.Data;
        SimpleEvents.Instance.AddListener<ScreenViewChangedEvent>(screenViewChanged);
    }
}
